drop database if exists springmvc;

create database springmvc;

use springmvc;

grant all on springmvc.* to root@localhost identified by '';